package main

import (
	"fmt"
)

func main() {
	for i := 1; i <= 80; i += 1 {

		if i%10 == 0 {
			fmt.Print("\033[43m0\033[m")
		} else {
			fmt.Print(i % 10)
		}
	}
	fmt.Println("")
}
